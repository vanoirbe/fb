xquery version "3.1";
(: ------------------------------------------------------------------
   FB application
   
   Topic model 

   Contributor(s): Christine Vanoirbeek 

   September 2019
   ------------------------------------------------------------------ :)

import module namespace oppidum = "http://oppidoc.com/oppidum/util" at "../../oppidum/lib/util.xqm";

declare option exist:serialize "method=xml media-type=text/xml";

let $cmd := oppidum:get-command()
let $lang := $cmd/@lang
let $content := fn:doc(oppidum:path-to-ref())//Content[@Lang=$lang]

return 
  $content

