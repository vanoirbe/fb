<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:site="http://oppidoc.com/oppidum/site" xmlns="http://www.w3.org/1999/xhtml">

  <xsl:output method="xml" media-type="text/html" omit-xml-declaration="yes" indent="yes"/>


  <xsl:template match="/">
    <site:view>
      <site:content>
        <div class="row">
          <div class="span12">
            <h2>Validation report</h2>
            <xsl:apply-templates select="Report"/>
          </div>
        </div>
      </site:content>
    </site:view>
  </xsl:template>
  
  <xsl:template match="Report[not(child::*)]">
    <p>Valid</p>
  </xsl:template>

  <xsl:template match="Report">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="Name">
    <h4>
      <xsl:value-of select="."/>
    </h4>
  </xsl:template>
  
  <xsl:template match="message">
    <p>
      <xsl:value-of select="."/>
    </p>
  </xsl:template>
  
</xsl:stylesheet>
