<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:site="http://oppidoc.com/oppidum/site" xmlns="http://www.w3.org/1999/xhtml">

  <xsl:output method="xml" media-type="text/html" omit-xml-declaration="yes" indent="yes"/>


  <xsl:template match="/">
    <site:view>
      <site:content>
        <div class="row">
          <div class="span12">
            <xsl:apply-templates select="Root"/>
          </div>
        </div>
      </site:content>
    </site:view>
  </xsl:template>

  <xsl:template match="Root">
    <p>The <xsl:value-of select="."/> collection has been updated</p>
  </xsl:template>
  
</xsl:stylesheet>
