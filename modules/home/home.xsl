<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:site="http://oppidoc.com/oppidum/site" xmlns="http://www.w3.org/1999/xhtml">

  <xsl:output method="xml" media-type="text/html" omit-xml-declaration="yes" indent="yes"/>

  <xsl:param name="xslt.base-url">/</xsl:param>
  
  <xsl:template match="/">
    <site:view>
      <site:content>
        <xsl:apply-templates/>
      </site:content>
    </site:view>
  </xsl:template>
  
  <xsl:template match="Content">
    <section id="services">
      <div class="container">
        <div class="row text-center">
          <div class="col-md-6">
            <img src="{$xslt.base-url}/images/{Booking/Illustration}" width="103" height="102" alt=""/>
            <h4 class="service-heading"><xsl:value-of select="Booking/Title"/></h4>
            <xsl:apply-templates select="Booking/Parag"></xsl:apply-templates>
          </div>
          <div class="col-md-6">
            <img src="{$xslt.base-url}/images/{CheckingFlights/Illustration}" width="103" height="102" alt=""/>
            <h4 class="service-heading"><xsl:value-of select="CheckingFlights/Title"/></h4>
            <xsl:apply-templates select="CheckingFlights/Parag"></xsl:apply-templates>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-12">
            <xsl:apply-templates select="Advice"></xsl:apply-templates>
          </div>
        </div>
      </div>
    </section>
  </xsl:template>
  
  <xsl:template match="Parag">
    <p class="text-muted"><xsl:value-of select="."/></p>
  </xsl:template>
  
  <xsl:template match="Advice">
    <p class="text-muted"><xsl:value-of select="."/></p>
  </xsl:template>
   
</xsl:stylesheet>
