xquery version "1.0";
(: ------------------------------------------------------------------
   FB application

   Contributor(s): Christine Vanoirbeek 

   October 2019 
   ---------------------------------------------------------------- :)

import module namespace request="http://exist-db.org/xquery/request";
import module namespace validation="http://exist-db.org/xquery/validation";

import module namespace oppidum = "http://oppidoc.com/oppidum/util" at "../../../oppidum/lib/util.xqm";
import module namespace ajax = "http://oppidoc.com/oppidum/ajax" at "../../lib/ajax.xqm";

import module namespace globals ="http://fb.com/globals" at "../../lib/globals.xqm";
import module namespace utilities ="http://fb.com/utilities" at "../../lib/utilities.xqm";


declare option exist:serialize "method=xml media-type=text/xml";

(: ======================================================================
   Validates submitted data.
   Returns a list of errors to report or the empty sequence.
   ======================================================================
:)
declare function local:validate-submission( $form as element() ) as element()* {
  let $report := validation:jing-report($form, fn:doc('/db/www/fb/validation/forms/form-booking.rng'))
  let $errors:= if ($report/status = 'invalid') then $report else () 
  return $errors
};

let $m := request:get-method()
let $cmd := oppidum:get-command()
let $base-url := $cmd/@base-url
return
    if ($m = 'POST') then
      let $form := utilities:remove-empty-tags(oppidum:get-data())
      let $errors := local:validate-submission($form)
      return
      if (empty($errors)) then
          oppidum:throw-message('NOT-YET-IMPLEMENTED',())
      else 
         ajax:report-validation-errors($errors)
    else (: assumes GET :)
      <Display>
         <Form Id="Booking">
           <Template>{concat($base-url,'templates/booking?goal=create')}</Template>
           <Actions>
             <Cancel>
               <Redirect>home</Redirect>
             </Cancel>
           </Actions>
         </Form>
     </Display>
 