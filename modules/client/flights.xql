xquery version "3.1";
(: ------------------------------------------------------------------
   FB application
   
   Client flights model 

   Contributor(s): Christine Vanoirbeek 

   September 2019
   ------------------------------------------------------------------ :)
   
declare namespace xdb = "http://exist-db.org/xquery/xmldb";
import module namespace request="http://exist-db.org/xquery/request";

import module namespace oppidum = "http://oppidoc.com/oppidum/util" at "../../../oppidum/lib/util.xqm";
import module namespace user = "http://oppidoc.com/ns/xcm/user" at "../../lib/user.xqm";
import module namespace display = "http://oppidoc.com/ns/xcm/display" at "../../lib/display.xqm";

import module namespace globals = "http://fb.com/globals" at "../../../lib/globals.xqm";

declare option exist:serialize "method=xml media-type=text/xml";

let $cmd := oppidum:get-command()
let $base-url := $cmd/@base-url
let $lang := string($cmd/@lang)
let $current-user-id := user:get-current-person-id ()
let $purchases := fn:doc(oppidum:path-to-ref())//Person[Id = $current-user-id]/Information/Purchases/Purchase

return 
  <Flights>
    <!-- coming soon -->
  </Flights>
  

