xquery version "3.1";
(: --------------------------------------------------------------
   fb application
   
   Global information about application

   Contributor(s): Christine Vanoirbeek

   July 2019 
   -------------------------------------------------------------- :)

module namespace globals = "http://fb.com/globals";

(: Application name (rest), project folder name and application collection name :)

declare variable $globals:app-name := 'fb';
declare variable $globals:app-folder := 'projects';
declare variable $globals:app-collection := 'fb';

(: Configuration paths :)

declare variable $globals:mapping-resource :='/db/www/fb/config/mapping.xml';
declare variable $globals:dico-collection := '/db/www/fb/dictionaries';
declare variable $globals:database-entities-resource := '/db/www/fb/config/database.xml';
declare variable $globals:schema-collection :='/db/www/fb/validation/schema';
declare variable $globals:global-schema-resource :='/db/www/fb/validation/fb.rng';
declare variable $globals:generate-all-schema-transfo :='/db/www/fb/validation/generate-all-schema.xsl';
declare variable $globals:templates-uri := '/db/www/fb/templates';
declare variable $globals:application-uri := '/db/www/fb/config/application.xml';

(: Application path :)

declare variable $globals:global-information-collection :="/db/sites/fb/global-information";
declare variable $globals:access-rigths-resource :="/db/sites/fb/global-information/access-rights.xml";
declare variable $globals:navigation-resource :="/db/sites/fb/global-information/navigation.xml";
declare variable $globals:selectors-resource :='/db/sites/fb/global-information/selectors.xml';
declare variable $globals:ids-resource :='/db/sites/fb/global-information/ids.xml';

declare variable $globals:topics-collection :="/db/sites/fb/topics";
declare variable $globals:home-resource := '/db/sites/fb/topics/home.xml';

declare variable $globals:connections-collection :="/db/sites/fb/connections";

declare variable $globals:users-collection :='/db/sites/fb/persons';


declare variable $globals:globals-uri := '/db/www/fb/config/globals.xml';

declare function globals:app-name() as xs:string {
  $globals:app-name
};

declare function globals:app-folder() as xs:string {
  $globals:app-folder
};

declare function globals:app-collection() as xs:string {
  $globals:app-collection
};

(: deprecated :)
declare variable $globals:xcm-name := 'xcm';

declare function globals:doc-available( $name ) {
  fn:doc-available(fn:doc($globals:globals-uri)//Global[Key eq $name]/Value)
};

declare function globals:collection( $name ) {
  fn:collection(fn:doc($globals:globals-uri)//Global[Key eq $name]/Value)
};

declare function globals:doc( $name ) {
  fn:doc(fn:doc($globals:globals-uri)//Global[Key eq $name]/Value)
};


